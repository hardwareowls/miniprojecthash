﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace MiniProjectHash
{
    class Compute
    {


        public string hash(string filepath, string algorithm, bool echo)
        {
            if (algorithm == "MD5")
            {
                using (var md5 = MD5.Create())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("MD5 of " + filepath + ": ");
                    Console.ForegroundColor = ConsoleColor.White;
                    string hash = BitConverter.ToString(md5.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower();
                    Console.WriteLine(hash);
                    if(echo)
                        File.AppendAllText("MD5.txt",hash + Environment.NewLine);
                    return (hash);

                }
            }
            if (algorithm == "SHA1")
            {
                using (var sha1 = SHA1.Create())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("SHA1 of " + filepath + ": ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(BitConverter.ToString(sha1.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                    return (BitConverter.ToString(sha1.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                }
            }
            if (algorithm == "SHA256")
            {
                using (var sha256 = SHA256.Create())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("SHA256 of " + filepath + ": ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(BitConverter.ToString(sha256.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                    return (BitConverter.ToString(sha256.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                }
            }
            if (algorithm == "SHA384")
            {
                using (var sha384 = SHA384.Create())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("SHA384 of " + filepath + ": ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(BitConverter.ToString(sha384.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                    return (BitConverter.ToString(sha384.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                }
            }
            if (algorithm == "SHA512")
            {
                using (var sha512 = SHA512.Create())
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("SHA512 of " + filepath + ": ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(BitConverter.ToString(sha512.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                    return (BitConverter.ToString(sha512.ComputeHash(File.ReadAllBytes(filepath))).Replace("-", "").ToLower());
                }
            }
            return "Error";
        }
    }
}
