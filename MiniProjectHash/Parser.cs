﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace MiniProjectHash
{
    class Parser
    {
        public void ConfigParser()
        {
            StreamReader file = new StreamReader("config");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                if (!line.Contains("/"))
                {
                    if (line.Contains("Threads"))
                        Program.threads = cleaner(line).ToString();
                    else if (line.Contains("File"))
                    {
                        string[] temp = line.Split(':');
                        Regex rgx = new Regex(@"\s*\t*\n*[\""]*");
                        string temp2 = rgx.Replace(temp[1], "");
                        if (File.Exists(temp2))
                            Program.fileName = temp2;
                        else
                            Console.WriteLine("Could not find file..." + Environment.NewLine + "Check the name in the conf file and the path");
                    }
                    else if (line.Contains("Splits"))
                        Program.splits = cleaner(line).ToString();
                    else if (line.Contains("Priority"))
                        Program.priority = cleaner(line).ToString();
                    else if (line.Contains("MD5"))
                        Program.config.Add("MD5", cleaner(line));
                    else if (line.Contains("SHA1"))
                        Program.config.Add("SHA1", cleaner(line));
                    else if (line.Contains("SHA256"))
                        Program.config.Add("SHA256", cleaner(line));
                    else if (line.Contains("SHA384"))
                        Program.config.Add("SHA384", cleaner(line));
                    else if (line.Contains("SHA512"))
                        Program.config.Add("SHA512", cleaner(line));
                }
            }
        }
        static int cleaner(string str)
        {
            try
            {
                string[] temp = str.Split(':');
                Regex rgx = new Regex(@"[a-z]|[A-Z]|[\s]");
                string clean = rgx.Replace(temp[1], "");
                Console.WriteLine(temp[0] + " " + clean);
                return int.Parse(clean);

            }
            catch (Exception e)
            {
                //Console.WriteLine("An error has occured" + Environment.NewLine + e.ToString());
                return 0;
            }

        }

    }
}
