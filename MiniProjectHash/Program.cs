﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Collections;

namespace MiniProjectHash
{
    class Program
    {
        #region config
        public static Dictionary<string, int> config = new Dictionary<string, int>();
        public static string fileName;
        public static string splits;
        public static string priority;
        public static string threads;
        public static string[] hashes;
        #endregion
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Parser par = new Parser();
            Compute comp = new Compute();
            par.ConfigParser();
            foreach (var pair in config)
            {
                if (pair.Value != 0)
                {
                    comp.hash(fileName, pair.Key.ToUpper(), true);
                }
            }
            if(splits != "0")
                Splitter.file(fileName, int.Parse(splits) * 1024 * 1024, Environment.CurrentDirectory.ToString());

            string[] fileEntries = Directory.GetFiles(Environment.CurrentDirectory.ToString() + "\\" + "Slet mig");
            Stopwatch sw = new Stopwatch();
            Console.WriteLine("Press enter to try the multi threaded test");
            Console.ReadKey();
            sw.Start();
            Parallel.ForEach(fileEntries, (fileName) =>
            {
                string temp = comp.hash(fileName, "MD5", false);
                string[] tempName = fileName.Split('\\');
                if (temp != "Error")
                {
                    if (File.Exists("MD5.txt"))
                        hashes = File.ReadAllLines("MD5.txt");
                    //if (hashes.Contains(temp))
                    //    Console.WriteLine(tempName[9] + " is not damaged! From thread: " +  Thread.CurrentThread.ManagedThreadId);
                    //else
                        Console.WriteLine(tempName[9] + " is damaged, please replace! From thread: " + Thread.CurrentThread.ManagedThreadId);
                }
            });
            Console.WriteLine("Elapsed time = " + sw.ElapsedMilliseconds.ToString() + "ms");
            Console.WriteLine("Press enter to start the single threaded test");
            Console.ReadKey();
            sw.Restart();
            foreach(string fileName in fileEntries)
            {
                string temp = comp.hash(fileName, "MD5", false);
                string[] tempName = fileName.Split('\\');
                if (temp != "Error")
                {
                    if (File.Exists("MD5.txt"))
                        hashes = File.ReadAllLines("MD5.txt");
                    //if (hashes.Contains(temp))
                    //    Console.WriteLine(fileName + " is not damaged");
                   //else
                        Console.WriteLine(tempName[9] + " is damaged, please replace");
                }                
            }
            Console.WriteLine("Elapsed time = " + sw.ElapsedMilliseconds.ToString() + "ms");
            Console.ReadKey();
        }
    }
}
